#!/usr/bin/env python3
from bagua.torch_api import allreduce
from bagua.torch_api.communication import broadcast_object
from bagua.torch_api.bucket import BaguaBucket
from bagua.torch_api.tensor import BaguaTensor
from typing import Any, Callable, Dict, List, Optional
from bagua.torch_api.data_parallel.bagua_distributed import BaguaDistributedDataParallel
from bagua.torch_api.algorithms.base import Algorithm, AlgorithmImpl
from bagua.torch_api.communication import BaguaProcessGroup
import math
import time
import torch
import torch.distributed

class GradientAllReduceAlgorithmImpl(AlgorithmImpl):
    def __init__(
        self,
        process_group: BaguaProcessGroup,
        hierarchical: bool = False,
        average: bool = True,
    ):
        """
        Implementation of the
        `GradientAllReduce <https://tutorials.baguasys.com/algorithms/gradient-allreduce>`_
        algorithm.

        Args:
            process_group (BaguaProcessGroup): The process group to work on.
            hierarchical (bool): Enable hierarchical communication.
            average (bool): If ``True``, the gradients on each worker are averaged.
                Otherwise, they are summed.
        """
        super(GradientAllReduceAlgorithmImpl, self).__init__(process_group)
        self.hierarchical = hierarchical
        self.average = average
        self.communicate = False #Boolean indicating if workers should communicate in the interval in question.
        self.LR = 0.2   #Initial learning rate
        self.LR_neo = 0.2  #Current learning rate
        self.CI = 10    #Inital Communication Interval
        self.CI_neo = 10    #Current Communication Interval
        self.gamma = 0.5
        self.loss = 1  #Inital Objective Function Loss
        self.loss_neo =1   #Current Objective Function Loss
        self.start_time = time.time()
        self.time_update_interval = 60 #Seconds between updates to communication interval
        self.train_step = 0
        self.warm_up = True
        self.variable_lr = False
        self.send_tensor = torch.zeros(1, device=torch.cuda.current_device())
        self.recv_tensor = torch.zeros(1, device=torch.cuda.current_device())

    def init_forward_pre_hook(self, bagua_ddp: BaguaDistributedDataParallel):
        def hook(input):
            self.train_step = bagua_ddp.bagua_train_step_counter-1
            if self.train_step == 0:
                self.start_time = time.time()
            if self.train_step%self.CI_neo == 0:
                self.communicate = True
            else:
                self.communicate = False          

        return hook
    
    def init_post_backward_hook(self, bagua_ddp: BaguaDistributedDataParallel):
        def hook():
            bagua_ddp._bagua_backend.wait_pending_comm_ops()
            if time.time() > self.start_time + self.time_update_interval and self.train_step%self.CI_neo==0 and not self.warm_up  :
                self.start_time = time.time()
                self.send_tensor = torch.tensor([self.loss_neo], device=torch.cuda.current_device())
                allreduce(self.send_tensor,self.recv_tensor,op = 10)
                self.loss_neo = self.recv_tensor.item()
                if self.variable_lr == True:
                    self.CI_neo =math.ceil(math.sqrt((self.LR*self.loss_neo)/(self.LR_neo*self.loss))*self.CI)
                else:
                    temp = math.ceil(math.sqrt((self.loss_neo)/(self.loss))*self.CI)
                    if temp < self.CI_neo:
                        self.CI_neo = temp
                    else:
                        self.CI_neo = math.ceil(self.CI_neo*self.gamma)
                self.CI_neo = broadcast_object(self.CI_neo)

        return hook
    
    
    def set_fixed_CI(self,bool): #If True the communication interval will remain unchanged throughout the training
        self.warm_up = bool

    def set_start_time(self,updated_start_time):
        self.start_time = updated_start_time

    def update_CI(self,updated_CI):
        if self.train_step == 0:
            self.CI = updated_CI
            self.CI_neo = updated_CI
        else:
            self.CI_neo = updated_CI

    def update_loss(self,updated_loss):
        if self.train_step == 0:
            self.loss = updated_loss
            self.loss_neo = updated_loss
        else:
            self.loss_neo = updated_loss

    def update_LR(self,updated_LR):
        self.variable_lr=True
        if self.train_step == 0:
            self.LR = updated_LR
            self.LR_neo = updated_LR
        else:
            self.LR_neo = updated_LR

    def init_backward_hook(self, bagua_ddp: BaguaDistributedDataParallel):
        def hook(parameter_name, parameter):
            if self.communicate: 
                if parameter_name in self._communication_tensor_names:
                    parameter.bagua_mark_communication_ready()

        return hook

    def init_tensors(
        self, bagua_ddp: BaguaDistributedDataParallel
    ) -> List[BaguaTensor]:
        parameters = bagua_ddp.bagua_build_params()
        tensors = []
        for name, param in parameters.__reversed__():
            p = param.ensure_bagua_tensor(name, bagua_ddp.bagua_module_name)
            tensors.append(p)
        self._communication_tensor_names = set(name for name, _ in parameters)

        return tensors


    def init_operations(
        self,
        _: BaguaDistributedDataParallel,
        bucket: BaguaBucket,
    ):
        bucket.clear_ops()
        bucket.append_centralized_synchronous_op(
            hierarchical=False,
            average=True,
            group=self.process_group,
        )


class GradientAllReduceAlgorithm(Algorithm):
    def __init__(self, hierarchical: bool = False, average: bool = True, variable_lr:bool = False):
        """
        Create an instance of the
        `GradientAllReduce <https://tutorials.baguasys.com/algorithms/gradient-allreduce>`_
        algorithm.

        Args:
            hierarchical (bool): Enable hierarchical communication.
            average (bool): If ``True``, the gradients on each worker are averaged.
                Otherwise, they are summed.
        """
        self.hierarchical = hierarchical
        self.average = average
        self.variable_lr = variable_lr


    def reify(self, process_group: BaguaProcessGroup) -> GradientAllReduceAlgorithmImpl:
        return GradientAllReduceAlgorithmImpl(
            process_group,
            hierarchical=self.hierarchical,
            average=self.average,
        )
