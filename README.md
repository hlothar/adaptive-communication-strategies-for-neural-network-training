# Adaptive Communication Strategies for Multi-Processor Neural Network Training

## Installing Adacomm
1. Install Bagua from https://tutorials.baguasys.com/installation
2. Find your local site-package manager where the bagua installation is located and delete the gradient_allreduce.py file saved under torch_api/algorithms/
3. Insert the gradient_allreduce.py file found in thie Git repository at the same location.

## Usage
To be able to use the code there are good explanations under https://tutorials.baguasys.com/getting-started/. Implementation specific user choices:

1. The standard initial communication interval is 10. If you want to change this use the command after initalizing the your model with Bagua
```sh
model_name.bagua_algorithm.update_CI(desired_communication_interval)
```
You have to pass the desired commmunication interval during the training process to the function as an argument.

2. By default the communication interval will not change adaptively during the training process to enable this use the command after initalizing the your model with Bagua
```sh
model_name.bagua_algorithm.set_fixed_CI(False)
```

3. If you enable adaptive communication in step 2, you will need to incorporate the following commands into your training loop, such that the best possible communication interval can be determined during the training process
```sh
model_name.bagua_algorithm.update_loss(current_objective_function_loss)
```
You have to pass the current objective function loss during the training process to the function as an argument.

4. If you want to use an adaptive learning rate, the function which determines the optimal communication interval during the training process changes and requires the knowledge about the value of the learning rate throughout the training process. To do this use the command below in your training loop
```sh
model_name.bagua_algorithm.update_LR(current_learning_rate)
```
You have to pass the current learning rate during the training process to the function as an argument.


## References 
1. https://github.com/BaguaSys/bagua
2. https://arxiv.org/abs/1810.08313
